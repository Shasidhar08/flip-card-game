let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
let images = [
  "chameleon.png",
  "dog.png",
  "dolphin.png",
  "fox.png",
  "koala.png",
  "lion.png",
  "turtle.png",
  "whale.png",
];
let btn = document.querySelector(".btn");
let counter = document.querySelector(".moves");
let time = document.querySelector(".time");
let items = document.querySelector(".items-container");
let card_back = document.querySelectorAll(".card-back");
let map = {};
let boolArray = new Array(16).fill(null);
let moves = 0,
  seconds = 0,
  matches = 0;
let interval;

const controller = new AbortController();
const signal = controller.signal;

function timer() {
  if (!interval) {
    interval = setInterval(() => {
      time.innerHTML = `time: ${seconds} ${seconds < 2 ? "Second" : "Seconds"}`;
      seconds++;
    }, 1000);
  }
}
function startGame(arr) {
  return cardsMap(arr);
}
function cardsMap(arr) {
  // console.log(arr);
  for (let i = 0; i < arr.length; i += 2) {
    map[arr[i]] = arr[i + 1];
    map[arr[i + 1]] = arr[i];
    card_back[i].parentNode.setAttribute("id", i + 1);
    card_back[i + 1].parentNode.setAttribute("id", i + 2);
  }
  // console.log(map);
  return fillImages(arr);
}
function fillImages(arr) {
  let i = 0;

  arr.map((ele) => {
    let back_img = document.createElement("img");
    let eleId = document.getElementById(ele);
    back_img.src = `images/${images[Math.floor(i / 2)]}`;
    back_img.style.width = "100%";
    back_img.style.objectFit = "fill";
    back_img.style.backgroundColor = "white";
    back_img.style.borderRadius = "1.5rem";
    back_img.style.display = "flex";
    eleId.appendChild(back_img);
    i++;
  });
}

let arr1 = [];

function match(id) {
  // console.log(event.target.parentNode);
  let matchIndex = map[id];
  let len = arr1.length;
  moves++;
  counter.innerHTML = `${moves} ${moves < 2 ? "Move" : "Moves"}`;
  if (len % 2 == 0) {
    arr1.push(matchIndex);
  } else {
    arr1.push(id);
  }
  len = arr1.length;
  boolArray[id - 1] = false;
  // console.log("array length: " + (len + 1));
  if (len > 1 && len % 2 == 0) {
    console.log(arr1);
    let prevMatch = arr1[len - 2];

    if (prevMatch === id) {
      console.log("Matched");
      boolArray[id - 1] = true;
      boolArray[map[prevMatch] - 1] = true;
      matches++;
      if (matches == 8) {
        finished();
      }
      // console.log(boolArray);
    } else {
      setTimeout(() => {
        document.getElementById(id).classList.toggle("flip");
        document.getElementById(map[prevMatch]).classList.toggle("flip");
      }, 500);
      boolArray[id - 1] = null;
      boolArray[map[prevMatch] - 1] = null;
      console.log("not matched");
    }
  }
}
function finished() {
  controller.abort();
  clearInterval(interval);
  counter.innerHTML = `Finished in ${moves} Moves`;
  time.innerHTML = `Time Taken : ${seconds} Seconds`;
}

function start(event) {
  event.target.textContent = "Restart";
  event.target.addEventListener("click", restart);
  // console.log("hello");
  array = array.sort(() => 0.5 - Math.random());
  startGame(array);
  timer();
  items.addEventListener("click", itemListener, { signal });
}
function itemListener(event) {
  let id = Number(event.target.parentNode.getAttribute("id"));

  // console.log(event.target.getAttribute("class"));
  if (
    event.target.getAttribute("class") != "items-container" &&
    boolArray[id - 1] === null &&
    boolArray[id - 1] !== false
  ) {
    event.target.parentNode.classList.toggle("flip");
    match(id);
  }
  // console.log(this.parentNode.getAttribute("id"));
}
function restart() {
  window.location.reload(true);
}

if (btn.textContent === "Start") {
  btn.addEventListener("click", start, { once: true });
}
